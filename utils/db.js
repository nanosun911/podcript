/**
 * Module dependencies.
 */
var debug = require('debug')('podcript:sqlite');
// database
const bsqlite3 = require('better-sqlite3')
const DB_PATH = './sqlite.db'

const DB = new bsqlite3(DB_PATH); //, { verbose: console.log }

DB.pragma('foreign_keys = ON;',  { simple: true });
console.log('Connected to ' + DB_PATH + ' database.')

const dbSchema = `CREATE TABLE IF NOT EXISTS Feeds (
        id text NOT NULL UNIQUE,
        type text NOT NULL DEFAULT yt,
        title text NOT NULL,
        link text NOT NULL,
        refresh_interval integer DEFAULT 24 NOT NULL,
        last_update text
    );

    CREATE TABLE IF NOT EXISTS Items (
        id text NOT NULL UNIQUE,
        title text,
        feed_id text NOT NULL,
        type text NOT NULL,
        description text,
        link text NOT NULL,
        pubdate text,
            FOREIGN KEY (feed_id) REFERENCES Feeds(id)
    );`

DB.exec(dbSchema)
console.log('db: created schema')
module.exports = DB
