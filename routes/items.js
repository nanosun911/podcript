var debug = require('debug')('podcript:items');
var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed
const fs = require('fs');

function getFeedItems (urlfeed, callback) {
	var req = request (urlfeed);
	var feedparser = new FeedParser ();
	var feedItems = new Array ();
	req.on ("response", function (response) {
		var stream = this;
		if (response.statusCode == 200) {
			stream.pipe (feedparser);
			}
		});
	req.on ("error", function (err) {
		console.log ("getFeed: err.message == " + err.message);
		});
	feedparser.on ("readable", function () {
		try {
			var item = this.read (), flnew;
			if (item !== null) { //2/9/17 by DW
				feedItems.push (item);
				}
			}
		catch (err) {
			console.log ("getFeed: err.message == " + err.message);
			}
		});
	feedparser.on ("end", function () {
		callback (undefined, feedItems);
		});
	feedparser.on ("error", function (err) {
		console.log ("getFeed: err.message == " + err.message);
		callback (err);
		});
	}

function showItemsPage(req,res){
	var feed_id = req.params.feed_id;
	const message = req.query.message || undefined;
	try {
		const feed = db.prepare('SELECT * FROM Feeds WHERE id = ? ').get(feed_id);
		if (feed === undefined) {throw 'Feed not found'};
		const rows = db.prepare('SELECT * FROM Items WHERE feed_id = ? ORDER BY rowid DESC ').all(feed_id);
		fs.readdir('./cache/', function(err, files) {
			res.render(
				'items.ejs',
				{
					title: 'Videos in '+feed.title,
					items: rows,
					feed: feed,
					cachefile: files,
					message: message
				});
		});
	}
	catch(err) {
		console.log(err);
		res.redirect(process.env.BASEURL+'?message='+err);
	}
}

function deleteItemsFile(id) {
	try {
		let path = './cache/'+id+'.mp4';
		if (fs.existsSync(path)) {
			fs.unlink(path, (err) => {
				if (err) {
						debug("failed to delete "+ path +' ' + err);
				} else {
						console.log('successfully deleted ' + path);
				}
			});
		}
		path = './cache/'+id+'.jpg';
		if (fs.existsSync(path)) {
			fs.unlink(path, (err) => {
				if (err) {
						debug("failed to delete "+ path +' ' + err);
				} else {
						console.log('successfully deleted ' + path);
				}
			});
		}
		path = './cache/'+id+'.webm';
		if (fs.existsSync(path)) {
			fs.unlink(path, (err) => {
				if (err) {
						debug("failed to delete "+ path +' ' + err);
				} else {
						console.log('successfully deleted ' + path);
				}
			});
		}
	} catch(err) {
		console.error(err)
	}
}

function deleteItemsThumb(id) {
	try {
		let	path = './cache/thumbs/'+id+'.jpg';
		if (fs.existsSync(path)) {
			fs.unlink(path, (err) => {
				if (err) {
						debug("failed to delete "+ path +' ' + err);
				} else {
						console.log('successfully deleted ' + path);
				}
			});
		}
	} catch(err) {
		console.error(err)
	}
}

module.exports = {
    itemsPage: (req, res) => {
			showItemsPage(req,res);
    },
    // refreshItems: (req, res) => {
    //   let feed_id = req.params.feed_id;
		// 	refreshItemsCall(req,res,feed_id);
		// 	showItemsPage(req,res);
    // },
		// refreshItemsCall: refreshItemsCall,
		deleteItemsFile: deleteItemsFile,
		deleteItemsFileRouter: (req, res) => {
			deleteItemsFile(req.params.id);
			backURL=req.header('Referer') || process.env.BASEURL;
			res.redirect(backURL);
    },
		deleteItemsThumb: deleteItemsThumb,
		getFeedItems: getFeedItems
		// deleteFeedFiles: deleteFeedFiles
};
