var debug = require('debug')('podcript:interval');

module.exports = {
    intervalRouter: (req, res) => {
      try {
        const feed_id = req.params.id;
        const interval = Math.floor(req.body.interval);
        debug('interval', feed_id, interval)
				const info = db.prepare('UPDATE Feeds SET refresh_interval = ? WHERE id = ? ').run(interval, feed_id);
        if (info.changes<1) {
          throw "error updating db:interval"
        } else {
          debug("updated interval to ", interval);
        }
        res.redirect(process.env.BASEURL+'?gmesg=update success');
      }
      catch(err){
        console.log(err);
        res.redirect(process.env.BASEURL+'?message='+err);
      }
    },
};
