var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  const message = req.query.message || undefined;
  const gmesg = req.query.gmesg || undefined;
  try {
    const rows = db.prepare('SELECT * FROM Feeds ORDER BY id ASC ').all();
    res.render('index', { title: 'Podscript | Video to podcast',
                          feeds: rows,
                          message: message,
                          gmesg: gmesg
                         });
  }
  catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});


module.exports = router;
