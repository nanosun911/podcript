#!/usr/bin/env node
var debug = require('debug')('podcript:thumbs');
var shell = require('shelljs');
const fs = require('fs');

function downloadThumbID(id, callback){
	const path = './cache/'+id+'.jpg'
	try {
		if (!fs.existsSync(path)) {
			//file does not exist
			let item = db.prepare('SELECT * FROM Items WHERE id = ? ').get(id);
			if (item) {
				let yttype = item.type.split(':')[0] || 'others';
				if (yttype == 'yt') return;
				var cmd = 'youtube-dl --skip-download --write-thumbnail -o "./cache/thumbs/%(id)s.%(ext)s" '+ item.link
				shell.exec(cmd, {shell: '/bin/bash'}); //download thumb in sync mode
				if (callback) callback();
			}
		}
	} catch(err) {
		console.error(err)
		if (callback) callback(err);
	}
}

module.exports = {
	  // downloadRouter: (req,res) => {
		// 	downloadID(req.params.id);
		// },
		downloadThumbID: downloadThumbID,
    thumbRouter: (req,res) => {
			var id = req.query.id;
			const path = './cache/thumbs/'+id+'.jpg'
			downloadThumbID(id, () => {
				res.sendFile(path, {'root': '.'});
			})
		},
};
