#!/usr/bin/env node
var debug = require('debug')('podcript:download');
var shell = require('shelljs');
const fs = require('fs');

function downloadID(id){
	const path = './cache/'+id+'.webm'
	try {
		if (!fs.existsSync(path)) {
			//file does not exist
			//var cmd = 'youtube-dl --match-filter "!is_live" --extract-audio --audio-format mp3 -f "worst" -o "./cache/%(id)s.%(ext)s" https://www.youtube.com/watch?v=' + id
                  var cmd = 'youtube-dl --match-filter "!is_live" --min-views 2 -f 250/worstaudio[ext=webm] -4 -o "./cache/%(id)s.%(ext)s" https://www.youtube.com/watch?v=' + id;
			shell.exec(cmd, {shell: '/bin/bash'}, function(code, stdout, stderr) {
				debug('Exit code:', code);
				debug('Program output:', stdout);
				debug('Program stderr:', stderr);
			});
		}
	} catch(err) {
		console.error(err)
	}
}

module.exports = {
	  // downloadRouter: (req,res) => {
		// 	downloadID(req.params.id);
		// },
		downloadID: downloadID,
		downloadRouter: (req,res) => {
			downloadID(req.params.id);
			backURL=req.header('Referer') || process.env.BASEURL;
			res.redirect(backURL);
		},
    sendFileRouter: (req,res) => {
			var id = req.query.id;
			const path = './cache/'+id+'.webm';
                        const mp4path = './cache/'+id+'.webm.part';
                        var checkEnd = function(mp4path) {
                                if (fs.existsSync(mp4path)) {
                                   debug("file is not ready, wait 500 ms...");
                                   setTimeout(checkEnd, 500, mp4path);
                                } else {
                                   debug("file is ready to download");
                                   res.download(path);
                                }
			    }
			try {
			  if (fs.existsSync(path)) {
			    //file exist
                             if (!fs.existsSync(mp4path)) {
					res.download(path);
                              } //else res.end();
			  } else {
					//var cmd = 'youtube-dl --extract-audio --audio-format mp3 -f "worst" -o "./cache/%(id)s.%(ext)s" https://www.youtube.com/watch?v=' + id;
                                   var cmd = 'youtube-dl --match-filter "!is_live" --min-views 2 -f 250/worstaudio[ext=webm] -4 -o "./cache/%(id)s.%(ext)s" https://www.youtube.com/watch?v=' + id;
					debug(cmd);
					shell.exec(cmd, {shell: '/bin/bash'}, function(code, stdout, stderr) {
					  debug('Exit code:', code);
					  //debug('Program output:', stdout);
					  debug('Program stderr:', stderr);
                                          if (stderr !== null && stderr !== '') {
                                             console.error(stderr, 'exiting');
                                             res.end()
                                          } else {
    					        setTimeout(checkEnd, 500, mp4path);	
						//var prevsize = fs.statSync(path).size;
                                                //debug(prevsize);
                                                //setTimeout(() => { console.log('timeout'); }, 300);
                                                //debug(fs.statSync(path).size);
						//while (fs.statSync(path).size !== prevsize) {
						//	prevsize = fs.statSync(path).size;
                                                //        debug(prevsize);
						//	debug("file size not stable", prevsize);
                                                //        setTimeout(() => {console.log('timeout');}, 300);
						//}
						//res.download(path);
                                            }
					});
				}
			} catch(err) {
		  	console.error(err)
			}
		},
};
