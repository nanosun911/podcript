var debug = require('debug')('podcript:feed');
var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed
const {getFeedItems, deleteItemsFile, deleteItemsThumb} = require('./items');
const {downloadID} = require('./download');
const {downloadThumbID} = require('./thumbs');

function getFeedMeta (urlfeed, callback) {
	var req = request (urlfeed);
	var feedparser = new FeedParser ();
	var feedItems = new Array ();
	req.on ("response", function (response) {
		var stream = this;
		if (response.statusCode == 200) {
			stream.pipe (feedparser);
			}
		});
	req.on ("error", function (err) {
		console.log ("getFeed: err.message == " + err.message);
		});
	feedparser.on ("meta", function (meta) {
		callback (undefined, meta);
		});
	feedparser.on ("error", function (err) {
		console.log ("getFeed: err.message == " + err.message);
		callback (err);
		});
	}

  function deleteFeedFiles(feed_id, offset, offset_thumb){
  	let rows = db.prepare('SELECT id from Items WHERE feed_id = ? ORDER BY rowid DESC LIMIT -1 OFFSET ? ').all(feed_id,offset);
		rows.forEach((row, index) =>{
			deleteItemsFile(row.id);
		});
    let rowsthumb = db.prepare('SELECT id from Items WHERE feed_id = ? ORDER BY rowid DESC LIMIT -1 OFFSET ? ').all(feed_id, offset_thumb);
		rowsthumb.forEach((row, index) =>{
			deleteItemsThumb(row.id);
		});
  }

  function refreshFeedbyID(feed_id){
  	let feed = db.prepare('SELECT * FROM Feeds WHERE id = ? ').get(feed_id);
		if (feed === undefined) {throw 'Feed not found'};
		getFeedItems(feed.link, function (err, items) {
			if (err) {
				console.error(err);
			} else {
        //update last_update field
        var date = new Date();
        let info = db.prepare('UPDATE Feeds SET last_update = ? WHERE id = ? ').run(date.getTime(), feed_id);
      	if (info.changes<1) {
      		throw "error updating db:refresh_time"
      	} else {
          debug("updated at: ", date, date.getTime());
				}
				//insert items in a database transaction
				items.reverse();
				for (let item of items) {
					//debug('outside',item.title); //debug use
					item.id = item.guid.split(':')[2];
					item.feed_id = feed_id;
				  //title is Title
					item.description = item['media:group']['media:description']['#'];
					//pubdate=pubdate
					//link is unlink
					item.type = feed.type
				}

				const insertsql = db.prepare('INSERT OR IGNORE INTO Items (id, feed_id, title, description, pubdate, link, type) VALUES (?,?,?,?,?,?,?) ');

				const insertRows = db.transaction((rows) =>{
					for (const row of rows) {
						insertsql.run(row.id, row.feed_id, row.title, row.description, row.pubdate.getTime(), row.link, row.type);
						//debug('in transaction', row.title);
					}
				});
				try {
					insertRows(items);
				}
				catch(err){
					if (!err.message.includes('UNIQUE constraint')) console.error(err);
				}

        //download latest file
        downloadID(items[items.length-1].id);
        downloadID(items[items.length-2].id);
        downloadID(items[items.length-3].id);
				for (const row of items) {
					downloadThumbID(row.id);
				}
  			//delete old files and entries
  		//keep 5 file and 20 images
  			deleteFeedFiles(feed_id, 6, 20)
        //delete old sql entries >20
				let deletinfo = db.prepare('DELETE FROM Items WHERE rowid IN (SELECT rowid FROM Items WHERE feed_id = ? ) ORDER BY rowid DESC LIMIT -1 OFFSET 20 ').run(feed_id);
				debug("Delete: Last ID: " + deletinfo.lastInsertRowid + "; # of Changes: " + deletinfo.changes);
			}
		});
  }

module.exports = {
    addFeedPage: (req, res) => {
        res.render('addFeed.ejs', {
            title: "Add a new Feed",
            message: ''
        });
    },
    addFeed: (req, res) => {
      let message = '';
      let feedurl = req.body.feedurl;
      getFeedMeta(feedurl, function (err, meta) {
      	if (err) {
          return res.status(500).send(err);
        }
        const idarray=meta['atom:id']['#'].split(':') //yt:playlist:id
        const type = idarray[0]+':'+idarray[1] || 'bl:channel'; //yt:playlist
        const title = meta.title;
        const id = idarray[2];  // id
        //check duplicate feed
				try {
        	const feedIDQuery = db.prepare("SELECT * FROM Feeds WHERE id = ? ");
					if (typeof feedIDQuery.get(id) !== 'undefined') {
						debug("feed already exists");
						message = 'Feed already exists';
						res.render('addFeed.ejs', {
								message: message,
								title: "Add a new Feed"
						});
					} else {
	          // add new feed
	          const sql = db.prepare("INSERT INTO Feeds (id, type, title, link) VALUES (?, ?, ?, ?) ");
	          const info = sql.run(id, type, title, feedurl);
	          debug("Last ID: " + info.lastInsertRowid + "; # of Changes: " + info.changes);
	          refreshFeedbyID(id)
	          res.redirect(process.env.BASEURL);
	        }
	    	}
				catch(err){console.log(err)}
			});
		},
    deleteFeed: (req, res) => {
      let id = req.params.id;
			try {
	      deleteFeedFiles(id, 0, 0)
	      const deleteItemInfo = db.prepare('DELETE FROM Items WHERE feed_id = ?').run(id);
				debug("Delete: Last ID: " + deleteItemInfo.lastInsertRowid + "; # of Changes: " + deleteItemInfo.changes);
				const deleteFeedInfo = db.prepare('DELETE FROM Feeds WHERE id = ?').run(id);
				debug("Delete: Last ID: " + deleteFeedInfo.lastInsertRowid + "; # of Changes: " + deleteFeedInfo.changes);
			}
			catch(err){console.log(err)}
      finally {res.redirect(process.env.BASEURL);}
    },
    refreshFeed: (req,res) => {
      let feed_id = req.params.feed_id;
			refreshFeedbyID(feed_id);
			//backURL=req.header('Referer') || process.env.BASEURL;
			res.redirect(process.env.BASEURL+'/items/'+feed_id);
    },
    refreshFeedbyID: refreshFeedbyID
};
