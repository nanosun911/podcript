var debug = require('debug')('podcript:opml');

module.exports = {
    opmlRouter: (req, res) => {
      try {
				const feeds = db.prepare('SELECT * FROM Feeds').all();

				let hostUrl = req.protocol + '://' + req.get('host');
				res.header('content-type', 'text/xml');
				res.render('opml.ejs', { feeds: feeds,
																hostUrl: hostUrl
															});
      }
      catch(err){
        console.log(err);
        res.redirect(process.env.BASEURL+'?message='+err);
      }
    },
};
