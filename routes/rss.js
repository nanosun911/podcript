var debug = require('debug')('podcript:rss');
const {refreshFeedbyID} = require('./feeds');

module.exports = {
    rssRouter: (req, res) => {
			  const feed_id = req.query.id;
        try {
				const feed = db.prepare('SELECT * FROM Feeds WHERE id = ?').get(feed_id);
        if (feed === undefined) {throw 'Feed not found'};
        const rows = db.prepare('SELECT * FROM Items WHERE feed_id = ? ORDER BY rowid DESC LIMIT 10 ').all(feed_id);
        let hostUrl = req.protocol + '://' + req.get('host');
        res.header('content-type', 'text/xml');
        res.render('rss.ejs', { feed: feed,
                                items: rows,
                                hostUrl: hostUrl
                              });
        }
        catch(err){
          console.log(err);
          backURL=req.header('Referer') || process.env.BASEURL;
    			res.redirect(backURL+'?message='+err);
        }
    },
};
