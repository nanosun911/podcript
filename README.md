# Podcript

This node application generates podcast feeds from youtube channels and playlists using [youtube-dl](https://ytdl-org.github.io/youtube-dl/). 

## Functions

+ add new youtube feed

  youtube playlist
  ```html
  https://www.youtube.com/feeds/videos.xml?playlist_id=$PLAYLISTID$
  ```
  
  youtube channels
  ```html
  https://www.youtube.com/feeds/videos.xml?channel_id=$CHANNELID$
  ```
  
+ batch export OPML file
+ export podcast RSS file
+ schedule auto feed refresh
+ cache/delete file
+ one lated video is automatically downloaded and converted to audio
+ three latest file per feed are kept on server and others are deleted
+ download webm

## Usage

- install [youtube-dl](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#installation)
- clone then `npm install`, `npm start`
- base URL and port can be changed by editing `.env` file, the default is:
  http://localhost:5263/podcript/

## Next
bilibili
