var createError = require('http-errors');
var express = require('express');
var path = require('path');
// var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
var logger = require('morgan');
const indexRouter = require('./routes/index');
const {addFeedPage, addFeed, deleteFeed, refreshFeed, refreshFeedbyID} = require('./routes/feeds');
const {sendFileRouter, downloadRouter} = require('./routes/download');
const {itemsPage, deleteItemsFileRouter} = require('./routes/items');
const {thumbRouter} = require('./routes/thumbs');
const {rssRouter} = require('./routes/rss');
const {opmlRouter} = require('./routes/opml');
const {intervalRouter} = require('./routes/interval');
var app = express();
var baseurlrouter = express.Router();
var multer  = require('multer')
var upload = multer()

console.log('PORT: ', process.env.PORT, 'BASEURL: ', process.env.BASEURL, "build: ", process.env.NODE_ENV);

//database
const db = require('./utils/db');
global.db=db

// setting for https/http
app.enable('trust proxy');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.disable('x-powered-by'); //prevent attack
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(process.env.BASEURL,express.static(path.join(__dirname, 'public')));
app.use(process.env.BASEURL+'/cache',express.static(path.join(__dirname, 'cache')));

baseurlrouter.use('/', indexRouter);
baseurlrouter.get('/delete/:id', deleteFeed);
baseurlrouter.get('/delete/:feed_id/:id', deleteItemsFileRouter);
baseurlrouter.get('/add', addFeedPage);
baseurlrouter.post('/add',  upload.array(), addFeed);
baseurlrouter.get('/download', sendFileRouter);
baseurlrouter.get('/download/:id', downloadRouter);
baseurlrouter.get('/items/:feed_id',  itemsPage);
baseurlrouter.get('/items/refresh/:feed_id',  refreshFeed);
baseurlrouter.get('/thumbs',  thumbRouter);
baseurlrouter.get('/rss',  rssRouter);
baseurlrouter.get('/opml',  opmlRouter);
baseurlrouter.post('/interval/:id',  intervalRouter);

app.use(process.env.BASEURL, baseurlrouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//schedule regular refresh
const intervalObj = setInterval(() => {
  try {
    const feeds = db.prepare('SELECT * FROM Feeds ORDER BY id ASC ').all();
    let nowtime = new Date().getTime();
    feeds.forEach((feed, index) =>{
      if ((nowtime-feed.last_update*1.0+index*300002)/3599999 > feed.refresh_interval){
        console.log('auto refreshing:', feed.title);
        refreshFeedbyID(feed.id);
      }
    });
  }
  catch (err) {console.log(err)}
}, 600001); //run every 10 min
console.log('Scheduled feed refresh job');

module.exports = app;
